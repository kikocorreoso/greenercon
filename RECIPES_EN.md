We love events, conferences, meetups,..., as are excellent ways to know amazing
and expert people from other sides of the world and to exchange experiences, 
meet collaborators and learn from others.

There are things we (event organisers, attendees, speakers, suppliers,...) can 
do in the course of planning an event that allow us to be more mindful of our 
footprint on the place where we live.

This is a list of informal ideas or suggestions to take into account in order to 
have a more environmental-friendly event.

If you could implement at least one of them it will be much better than 
doing nothing.

# Some notes before going forward

*   In this text, event, meetup, conference, workshop,..., are interchangeable 
    terms for the same, i.e., a meeting of people from different places. We 
    will try to use the *event* word for this.

*   In this text, environment friendly, eco-friendly, sustainable, green,..., 
    are interchangeable terms for the same, i.e., reduce the overall footprint
    of our event on the planet.

*   A participant could be whoever that goes to the event, speakers, sponsors,
    volunteers, attendees,... When we talk about attendees we are referring to
    any individual that can have an impact and, in general, we will call them 
    attendees in this document.

# TRAVELLING

## Attendee

*   If walking or biking is not an option choose the mode of transport with 
    lower carbon footprint. Ordered from lower to higher emissions: Train, 
    coach/bus, car, ship, plane 
    ([source](https://www.eea.europa.eu/media/infographics/co2-emissions-from-passenger-transport/view)).

*   If you plan to travel by car try to occupy all the seats. Try to know if 
    other people from your city will go to the conference and talk with them.
    Use sites like [blablacar](https://www.blablacar.com/). Talk with the event
    organiser in order to ask for a site where attendees can talk and organise
    the travel (a mail list, a forum,...).

## Organiser

*   Try to make a deal with railway companies to obtain discounts for the
    attendees.

*   Try to make a deal with coach/bus companies to obtain discounts for the
    attendees.

*   Promote the use of lower emission transportation. You could offer discounts 
    on trains, buses,... You could talk with sponsors to offer grants for 
    trains, buses,...

*   Create a site where attendees can organise the best way to go to the event.
    A simple mail list, forum or alternatives should be enough.

*   Streaming could be an option for some people. If you don't have enough 
    budget for streaming you could ask for a 'streaming ticket' to those people.

## Sponsor

*   Promote the use of lower emission transportation for your representatives
    travelling to the event.

*   Adjust correctly the size of the team needed at the event. If some people
    will take advantage of the conference because the topic is more interesting 
    for their daily tasks then try to support that people to be present within 
    the team and try to avoid people that will not take as much advantage of the 
    event.

# ACCOMMODATION

## Attendee

*   Choose hotel/hostels/accommodation companies that are involved in green 
    initiatives and/or that promotes environmental “best practices”.

*   Choose accommodation where you minimise the journey time during the event
    period.

## Organiser

*   Try to make a deal with accomodation companies with a green policy to obtain 
    discounts for the attendees/speakers/sponsors.

*   Choose a venue that is not far from hotels, hostels, city center,...

# LOCAL TRANSPORT DURING THE EVENT

## Attendee

*   If walking or biking is not an option choose the available public transport.

*   If you plan to go walking or biking you can try to meet with others and 
    spend that time knowing new people and having insightful conversations. Ask
    the event organisation to provide tools to do so.

## Organiser

*   Try to centralise the local transport information on a site that it is easy 
    to find and provide clear and concise information.

*   Try to offer a site where people can organise the way to arrive to the 
    event. For instance, if there are some people sleeping in an area not so 
    well communicated and this people will use private cars they could try to 
    find other people in the same situation and reduce the number of needed 
    cars.

*   Try to make deals with bike rental companies to obtain discounts for the
    attendees.

# TICKET

## Attendee

*   Accept that sometimes something more expensive is not a waste of money, is 
    an investment in health, in beautiful landscapes, more livable places to 
    live,...

## Organiser

*   Offer the attendees the possibility to customise their experience during the
    ticket process. They could choose if they want a T-shirt of the event, if 
    they can live without plastic bottles or cans during the meals,...

# WELCOME PACK

## Attendee

*   If you could reuse a lanyard from a past event just travel with it.

*   If you could reuse a notebook and pen from a past event or from your home
    just travel with it.

*   If you do not like the T-shirt design or do you think it is not necessary
    reject it.

*   If you can check the event's programme from your mobile do not accept a
    printed programme.

*   If you find useless merchandising from sponsors try to return the physical 
    stuff to the sponsor so they could use it in future events.

*   If you find useless printed information/flyers try to think the best way to 
    make the most of it (e.g., write on the clean side). When you are done try
    to recycle the paper.

## Organiser

*   Offer the possibility to refuse the Welcome Pack or parts of it.

*   Tell in advance, if possible, what will be included in the Welcome Pack.

*   If there will be a T-shirt or other kind of swag of the event try to show 
    the design in advance so the attendees could refuse it if they do not 
    needed it.

*   Offer alternatives to the swag like a local seasonal fruit.

*   Work with merchandising companies with eco-friendly and acceptable labor 
    policies.

*   Offer the sponsors alternatives to paper. E.g., in case the attendees want 
    to access the conference's wifi the attendees should visit webpage X where 
    they could obtain the wifi credentials but first they will see a richer,
    not obtrusive, privacy friendly message from the sponsor (interactive, with 
    the possibility to update the message if there is something wrong,...). 
    E.g., offer the possibility to include some sponsor information on 
    newsletters or other kind of communication you have with the attendees.

## Sponsor

*   Try to avoid papers and to think in alternatives to get most of your 
    sponsorship, be creative and environmental-friendly.

*   Talk with the organiser to include the flyer information within the event's
    web page.

*   If it is in your hands try to offer a service instead of something physical 
    and low quality.

*   Talk with other sponsors to check if you could join efforts.

*   If you want to offer something physical think in something that is useful 
    for the attendee and is made locally and not from the other side of the 
    world. E.g., generally speaking a notebook or a pen could be not useful as 
    most of the attendees already have tons of them.

# BOOTH

## Attendee

*   Go to the sponsor's area to find the amazing stuff that the companies are 
    doing. The event is possible thanks to them.

*   If a sponsor is offering some merchandising think if you really need it or
    if it will end up in a trash bin.

## Organiser

*   Offer time of the conference to the sponsors.

*   Locate the sponsor's area in a strategic placewithin the venue.


## Sponsor

*   Try to offer useful and practical merchandising that would be used during
    long time after the conference.

*   Try to look for merchandising made locally, with recicled or organic 
    materials, that can be recycled and that was made in decent work conditions.
    Even some simple things like, e.g., a local transport ticket, a local fruit,
    ..., could be excellent merchandising for your company.

# VENUE SELECTION

## Organiser

*   Try to choose those with better eco-friendly policies. Try to get 
    information about if they have recicle bins, a kitchen and a dinning room
    with china to avoid plastic plates, glasses,..., drinking fountains,...

*   Try to choose those with better location in order to reduce the total
    amount of displacements.

*   Try to have a only a venue to reduce the number of displacements.

*   Try to find a venue with restaurants and other useful services within 
    walking distance.

# FOOD

## Organiser

*   Choose a catering that offers local, fresh, seasonal and healthy food.

*   Ask the catering to use china, glasses, silverware, linens instead of 
    plastic/paper.

*   Ask the catering to offer water pitchers or stations instead of plastic 
    bottles.

*   Ask the catering to offer juices from local fruits instead of sodas that
    needs several liters of water to obtain a liter of liquid 
    ([source](https://www.coca-colacompany.com/stories/setting-a-new-goal-for-water-efficiency)
    and this only takes into account the liquid itself, not the manufacturing of 
    the plastic bottles or cans packaging, the sugar cane crops,...).

*   Ask the catering to offer local beverages like local beers, wines, sodas.

*   Organize the attendees to bring their own canteen or offer one as a swag in
    the welcome pack in case they do not own one.

*   Offer drinking fountains or water stations in several strategic points of 
    the venue so attendees can fill their canteens.

*   Choose menus with more vegetables, cereals, pasta, rice,... (try to
    avoid lamb and beef as much as possible, 
    [source](https://www.ewg.org/meateatersguide/a-meat-eaters-guide-to-climate-change-health-what-you-eat-matters/climate-and-environmental-impacts/)).

*   Try to have a policy about any food that is leftover. Ask If the venue or 
    catering have any practice to donate to a particular organization. Some 
    areas even have local food rescue teams ready to distribute to organizations 
    in need.

## Attendee

*   Bring your own canteen or metal bottle so you can use drinking fountains.

*   Ask for products with minimal packaging.

*   Ask for local, healthy and seasonal products.
   
# DURING THE EVENT

## Attendee

*   As you travelled from very far try to get the most of the event. Speak with
    people face to face, ask your doubts during the talks, spend some time on
    networking, do not use your social networks during the event's time,...

*   Use convenient clothes. E.g., if you are cold natured try to use thermal
    clothing instead asking to turn up the heat.

*   If you love something local, e.g., a local wine, try to enjoy it during the
    event's period instead of ordering from your daily's residence.

*   If you are the last leaving a room try to check if lights, projectors,..., 
    are switched off.

## Organiser

*   Offer warm spaces where people can speak and discuss.

*   Try to turn heaters/air conditioning to a convenient temperature.

*   Try to ask people about temperature at rooms.

*   As the idea is to have the most useful event try to organise other 
    activities or help people that want to make a sprint, a meeting,...

*   If you’ve made the effort to create an eco-friendly event, shout it out. Let 
    your participants know about your efforts and why you are doing so.

## Speaker

*   Be open and approachable. A lot of people travelled from very far to see
    you and to talk to you. Maybe we will get the next big thing from this
    encounter.

*   Try to spent as much time as possible in the event.

# AFTER THE EVENT

## Attendee

*   Try to think about what could be improved and send your feedback to the
    organisation.

## Organisation

*   Provide tools to the attendees in order to receive feedback.

*   Consider having a debrief with your team to assess how well you adhered to 
    your own sustainability policy. Is there room for improvements or changes 
    before the next event you plan?

# REFERENCES

http://www.in-conference.org.uk/environmental-policy

http://theeventu.com/individuals/blog/green-meetings-events-good-environment-good-business/

https://theeventu.com/individuals/blog/eco-friendly-events-pt-1/

https://theeventu.com/individuals/blog/eco-friendly-events-pt-2/

https://theeventu.com/individuals/blog/eco-friendly-events-pt-3/

https://theeventu.com/individuals/blog/eco-friendly-events-pt-4/

https://theeventu.com/individuals/blog/eco-friendly-events-pt-5/