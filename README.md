# GreenerCon

A cookbook to have a greener conference, meetup,...

# Why?

All of us are living in a spaceship travelling within the universe and is the
only one we have at this moment.

Also, because it's beautiful and 
[Beautiful is better than ugly](https://www.python.org/dev/peps/pep-0020/).

> When I orbited the Earth in a spaceship, I saw for the first time how 
> beautiful our planet is. Mankind, let us preserve and increase this beauty, 
> and not destroy it!
>
> Yuri Gagarin, Russian Cosmonaut

# Recipes

## Welcome Pack

* T-shirt
* Paper flyers
* Merchandising

## Food

* Plastic everywhere
* Local products

## Travelling to the conference

* Organizer announcements
* Sharing alternatives

## During the conference

* Be responsible with your time at the conference

# Notes to myself

https://www.eea.europa.eu/data-and-maps/daviz/specific-co2-emissions-per-passenger-3#tab-chart_1
http://gogreenconference.net/advisory_board/
https://duckduckgo.com/?q=water+footprint&t=lm&ia=web
https://duckduckgo.com/?q=carbon+footprint&t=lm&ia=web
